Collaboration -- Tools and Rules
================================

A hackathon is a team exercise. Below we highlight a few tools that a team participating in a hackathon might resort to.

# Brainstorming

For brainstorming and ideation, a tool such as [Trello](https://trello.com/en) can prove quite useful. It organises ideas on different boards, define priorities for them, assign to team members and set a deadline for delivery. You might also consider [Google Docs](https://www.google.com/docs/about/).

# Communication

For quick information, short discussions about a topic or assistance to complete a task, you might consider [Slack](https://slack.com/intl/en-na/) or [Discord](https://discord.com/). In case you use any of these tools, please ensure that you keep the channel among team members to avoid "disturbing" other teams.

# Code sharing

A typical code sharing protocol is [git](https://git-scm.com/), a decentralised version control system. For this hackathon, we wish to have the source code of all participating teams in different branches in this repository. As such, we introduce below a simple git **workflow**.

Each team should consolidate its source code under a __develop__ branch in the repo. Once the details of the teams are communicated, specific develop branches will be created and assigned (with appropriate permissions). While a team is working on a specific feature, it should create a branch based on its develop branch. For example, `git checkout -b feature2 develop1` for team 1 creating a branch for feature2. After the functionality in feature2 has been completed and tested, the content of feature2 should be merged with develop1 and feature2 deleted. The steps are described below:
```bash
git checkout develop1
git fetch
git merge --no-ff feature2
git branch -d feature2
git push origin develop1
```
While working on feature2, if a team member wishes to share it with another member, he/she might push it to the remote repository.
