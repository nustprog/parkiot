ReadMe
======
__parkiot__ is an effort by the Computer Science Department at [NUST](http://www.nust.na) to solve challenges associated with physical parking meters. If successfully implemented, parking fees should be paid electronically and even remotely from the parking bays
 a lot of admin reports can be easily generated such as to see the busiest sections of parking bays as well as the revenue generated over a choice period. The context is for open parking, where there is no fixed entrance as in shopping malls
